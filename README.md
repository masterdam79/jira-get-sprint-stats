# Jira Get Sprint Stats

Create config.txt in this directory as follows

```
[BASICAUTH]
JIRA_API_KEY = abcdefg
JIRA_USER = user@domain.tld

[DETAILS]
JIRA_URL = https://MYSITE.atlassian.net
PROJECT = PROJECTNAME
```
