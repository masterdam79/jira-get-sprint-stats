#!/usr/bin/env python3

import configparser
from jira import JIRA

# Get some variables outside this script
config = configparser.ConfigParser()
config.read('./config.txt')
jira_api_key = config['BASICAUTH']['JIRA_API_KEY']
jira_user = config['BASICAUTH']['JIRA_USER']
jira_url = config['DETAILS']['JIRA_URL']
jira_project = config['DETAILS']['PROJECT']

options = {
 'server': jira_url
}

jira = JIRA(options, basic_auth=(jira_user,jira_api_key) )

ticket = jira_project + '-2208'
issue = jira.issue(ticket)

summary = issue.fields.summary

print('ticket: ', ticket, summary)
